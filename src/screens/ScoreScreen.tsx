import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TextInput,
  TouchableHighlight,
  Text,
  View,
  Modal,
  Alert,
} from 'react-native';

import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

// import {State} from '../../store/types/types';
import * as Actions from '../../store/actions/action';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

interface Props {
  navigation: any;
}

const Hello: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const [value, onChangeText] = React.useState('');
  const [modalVisible, setModalVisible] = React.useState(true);
  const [results, setResults] = useState<number[]>([]);
  let tresults = useSelector((state) => state.reducer.playersBestTenResults);

  const getData = async () => {
    try {
      const res = await AsyncStorage.getItem('results');
      return res != null ? JSON.parse(res) : null;
    } catch (e) {
      // error reading value
    }
  };

  useEffect(() => {
    const setCurrentScores = async () => {
      let r = await getData();
      if (r !== null) {
        setModalVisible(false);
      }
    };
    setCurrentScores();
  }, []);

  const startNewGame = () => {
    dispatch(Actions.resetGame());
    props.navigation.navigate('GameScreen');
  };

  const buttonClicked = () => {
    if (value === '') {
      Alert.alert('Please enter your name...');
      return;
    }
    setResults(results);
    setModalVisible(false);
  };
  return (
    <View style={styles.root}>
      <View>
        <Modal
          presentationStyle={'formSheet'}
          animationType="slide"
          transparent={false}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            setModalVisible(!modalVisible);
          }}>
          <View style={styles.modalCon}>
            <View style={styles.textCon}>
              <Text style={{fontSize: wp('15%')}}>Good Job !</Text>
              <Text style={{fontSize: wp('7%')}}>Please Enter Your Name</Text>
            </View>
            <TextInput
              placeholder="Your Name ..."
              style={styles.textInput}
              onChangeText={(text) => onChangeText(text)}
              value={value}
            />
            <TouchableHighlight
              onPress={buttonClicked}
              style={styles.saveNameButtonCon}>
              <Text style={{fontSize: wp('5%')}}>Save Score !</Text>
            </TouchableHighlight>
          </View>
        </Modal>
      </View>

      <View style={styles.scoreScreen}>
        <Text style={{fontSize: wp('5%')}}>
          {value ? `${value}, Here are your top 10 results!` : 'No Name...'}
        </Text>
        {results !== null
          ? tresults.map((score: number, index: number) => (
              <Text style={styles.listText} key={index}>
                {`${index + 1}) ${score}`}
              </Text>
            ))
          : null}
      </View>
      <TouchableHighlight
        onPress={startNewGame}
        style={styles.startNewGameButton}>
        <Text>Start New Game</Text>
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  listText: {
    fontSize: wp('7%'),
  },
  textCon: {
    alignItems: 'center',
  },
  modalCon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  textInput: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: hp('5%'),
    width: wp('40%'),
    borderBottomWidth: 3,
    backgroundColor: 'white',
  },
  goodjob: {
    fontSize: wp('15%'),
  },
  saveNameButtonCon: {
    width: wp('70%'),
    height: hp('10%'),
    borderRadius: 20,
    marginTop: 20,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scoreScreen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  startNewGameButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'green',
    width: wp('100%'),
    height: hp('10%'),
  },
});

export default Hello;
