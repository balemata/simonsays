import React, {useState, useEffect, useRef} from 'react';
import {Animated, StyleSheet, Text, View, Platform} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import {useDispatch, useSelector} from 'react-redux';

// import Sound from 'react-native-sound';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import ButtonsGroup from '../components/ButtonsGrid/ButtonsGroup';
import StartGameButton from '../components/ButtonsGrid/StartGameButton';

import * as Actions from '../../store/actions/action';
import {State} from '../../store/types/types';

interface Props {
  navigation: any;
}

const GameScreen: React.FC<Props> = (props) => {
  // Colors state for animation
  let redX = useRef(new Animated.Value(0)).current;
  let greenX = useRef(new Animated.Value(0)).current;
  let blueX = useRef(new Animated.Value(0)).current;
  let yellowX = useRef(new Animated.Value(0)).current;

  var redColor = redX.interpolate({
    inputRange: [0, 1],
    outputRange: ['#ff0000', 'rgb(0,0,0)'],
  });

  var greenColor = greenX.interpolate({
    inputRange: [0, 1],
    outputRange: ['#16c716', 'rgb(0,0,0)'],
  });

  var blueColor = blueX.interpolate({
    inputRange: [0, 1],
    outputRange: ['rgb(0,0,250)', 'rgb(0,0,0)'],
  });

  var yellowColor = yellowX.interpolate({
    inputRange: [0, 1],
    outputRange: ['#f0e21b', 'rgb(0,0,0)'],
  });

  // Game State
  let computerSequence = useSelector(
    (state: State) => state.reducer.computerSequence,
  );
  const [playerSequence, setPlayerSequence] = useState<number[]>([]);
  const [results, setResults] = useState<number[]>([]);
  const [computerTurn, setComputerTurn] = useState<boolean>(true);
  const [gameInProgress, setGameInProgress] = useState<boolean>(false);
  const [level, setLevel] = useState<number>(0);

  const dispatch = useDispatch();

  useEffect(() => {
    const setCurrentSequence = async () => {
      // await AsyncStorage.clear();
      if (computerSequence.length === 0) {
        setLevel(0);
        setComputerTurn(true);
      }
      setPlayerSequence([...computerSequence]);
    };
    setCurrentSequence();
  }, [computerSequence, gameInProgress]);

  const randomNumberOneToFour = (): number => {
    return 1 + Math.floor(Math.random() * 4);
  };

  const storeResultOnAsyncStorage = async (scores: number[]) => {
    const jsonValue = JSON.stringify(scores);
    await AsyncStorage.setItem('results', jsonValue);
  };

  const onStartGamePressed = (): void => {
    if (gameInProgress) {
      console.log('yeah');
      setLevel(0);
      computerSequence = [];
      dispatch(Actions.resetGame());
    }
    setGameInProgress(true);
    computerPlay();
  };

  const buttonToAnimate = (randomNumber: number) => {
    switch (randomNumber) {
      case 1:
        animateButton(redX);
        break;
      case 2:
        animateButton(greenX);
        break;
      case 3:
        animateButton(blueX);
        break;
      case 4:
        animateButton(yellowX);
        break;
    }
  };

  const animateButton = (button: Animated.Value): void => {
    Animated.timing(button, {
      toValue: 1,
      duration: 200,
      useNativeDriver: false,
    }).start();
    setTimeout(() => {
      Animated.timing(button, {
        toValue: 0,
        duration: 200,
        useNativeDriver: false,
      }).start(() => {});
    }, 1000);
  };

  const animateButtonAndShift = (
    compSeq: number[],
    color: any,
    lastItem: number,
  ) => {
    Animated.timing(color, {
      toValue: 1,
      duration: 200,
      useNativeDriver: false,
    }).start();
    setTimeout(() => {
      Animated.timing(color, {
        toValue: 0,
        duration: 200,
        useNativeDriver: false,
      }).start(() => {
        compSeq.shift();
        buttonsToAnimate(compSeq, lastItem);
      });
    }, 1000);
  };

  const buttonsToAnimate = (compSeq: number[], lastItem: number) => {
    if (compSeq.length === 0) {
      buttonToAnimate(lastItem);
      setComputerTurn(false);
      return;
    }
    switch (compSeq[0]) {
      case 1:
        animateButtonAndShift(compSeq, redX, lastItem);
        break;
      case 2:
        animateButtonAndShift(compSeq, greenX, lastItem);
        break;
      case 3:
        animateButtonAndShift(compSeq, blueX, lastItem);
        break;
      case 4:
        animateButtonAndShift(compSeq, yellowX, lastItem);
        break;
    }
  };

  const computerPlay = (): void => {
    setComputerTurn(true);
    let randomNumber = randomNumberOneToFour();
    dispatch(Actions.addColorToComputerSequence(randomNumber));
    buttonsToAnimate(computerSequence, randomNumber);
  };

  const onButtonClicked = (buttonID: number): void => {
    if (playerSequence[0] !== buttonID) {
      dispatch(Actions.storePlayerScore(level));
      setResults([...results, level]);
      storeResultOnAsyncStorage(results);
      props.navigation.navigate('ScoreScreen');
    } else {
      // Sucessfully choose color
      playerSequence.shift();

      // User Successfully click the colors
      if (playerSequence.length === 0) {
        setPlayerSequence([...computerSequence]);
        setLevel((prev) => (prev = prev + 1));
        setComputerTurn(true);
        computerPlay();
      }
    }
  };

  return (
    <View style={styles.boardContainer}>
      <View style={styles.textCon}>
        <Text style={styles.welcomeText}>Have Fun</Text>
        <Text style={styles.score}>Current Score: {level}</Text>
      </View>
      <View style={styles.buttonsGroup}>
        <ButtonsGroup
          red={redColor}
          green={greenColor}
          blue={blueColor}
          yellow={yellowColor}
          computerTurn={computerTurn}
          onButtonClicked={onButtonClicked}
        />
      </View>
      <View style={styles.startGameButtonCon}>
        <StartGameButton
          gameInProgress={gameInProgress}
          onStartGamePressed={onStartGamePressed}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  boardContainer: {
    flex: 1,
  },
  textCon: {
    marginTop: Platform.OS === 'ios' ? hp('10%') : hp('4%'),
    alignItems: 'center',
  },
  welcomeText: {
    fontSize: wp('6.5%'),
  },
  buttonsGroup: {
    flex: 3,
    justifyContent: 'center',
  },
  startGameButtonCon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  score: {
    fontSize: wp('5%'),
  },
});

export default GameScreen;
