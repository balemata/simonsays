import React from 'react';
import {StyleSheet, Animated, TouchableHighlight} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

interface IProps {
  id: number;
  color: string;
  buttonClicked: Function;
  computerTurn: boolean;
}

const Button: React.FC<IProps> = (props): JSX.Element => {
  const buttonClicked = () => {
    props.buttonClicked(props.id);
  };

  return (
    <TouchableHighlight
      style={styles.containerStyle}
      key={props.id}
      disabled={props.computerTurn}
      activeOpacity={0.6}
      underlayColor="black"
      onPress={() => buttonClicked()}>
      <Animated.View
        style={[styles.containerStyle, {backgroundColor: props.color}]}
      />
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    borderWidth: 4,
    borderColor: 'black',
    width: wp('35%'),
    height: hp('15%'),
    borderRadius: wp('100%'),
  },
  animatedView: {
    width: wp('35%'),
    height: hp('15%'),
  },
});

export default Button;
