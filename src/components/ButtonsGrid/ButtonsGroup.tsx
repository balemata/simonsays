import React from 'react';
import {View, StyleSheet, Animated} from 'react-native';

import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Button from './Button';

type MaybeAnimated<T> = T | Animated.Value | Animated.AnimatedInterpolation;

interface IProps {
  red: MaybeAnimated<any>;
  green: MaybeAnimated<any>;
  blue: MaybeAnimated<any>;
  yellow: MaybeAnimated<any>;
  onButtonClicked: Function;
  computerTurn: boolean;
}

const ButtonsGroup: React.FC<IProps> = (props) => {
  const buttonClicked = (buttonID: string) => {
    props.onButtonClicked(buttonID);
  };

  return (
    <View style={styles.container}>
      <View style={styles.buttonRowOne}>
        <Button
          id={1}
          computerTurn={props.computerTurn}
          buttonClicked={buttonClicked}
          color={props.red}
        />
        <Button
          id={2}
          computerTurn={props.computerTurn}
          buttonClicked={buttonClicked}
          color={props.green}
        />
      </View>
      <View style={styles.buttonRowTwo}>
        <Button
          id={3}
          computerTurn={props.computerTurn}
          buttonClicked={buttonClicked}
          color={props.blue}
        />
        <Button
          id={4}
          computerTurn={props.computerTurn}
          buttonClicked={buttonClicked}
          color={props.yellow}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  buttonRowOne: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  buttonRowTwo: {
    marginTop: hp('10%'),
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});

export default ButtonsGroup;
