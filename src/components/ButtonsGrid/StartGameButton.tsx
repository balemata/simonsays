import React from 'react';
import {ViewStyle, Text, TouchableHighlight, StyleSheet} from 'react-native';

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export interface Props {
  onStartGamePressed: Function;
  gameInProgress: boolean;
}

interface Styles {
  buttonCon: ViewStyle;
}

const styles = StyleSheet.create<Styles>({
  buttonCon: {
    width: wp('50%'),
    height: hp('5.6%'),
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 20,
  },
});

const StartGameButton: React.FC<Props> = (props) => {
  return (
    <TouchableHighlight
      onPress={(): void => {
        props.onStartGamePressed();
      }}
      style={styles.buttonCon}>
      <Text>{props.gameInProgress ? 'Start New Game' : 'Start Game'}</Text>
    </TouchableHighlight>
  );
};

export default StartGameButton;
