/**
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 **/

import React from 'react';
import {ViewStyle} from 'react-native';

import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {enableScreens} from 'react-native-screens';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';

import {reducer} from './store/reducers/reducer';

import GameScreen from './src/screens/GameScreen';
import ScoreScreen from './src/screens/ScoreScreen';

const rootReducer = combineReducers({
  reducer,
});

export const store = createStore(rootReducer);

export interface Styles {
  root: ViewStyle;
}

enableScreens();
const Stack = createNativeStackNavigator();

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="GameScreen"
          screenOptions={screenOptionStyle}>
          <Stack.Screen name="GameScreen" component={GameScreen} />
          <Stack.Screen name="ScoreScreen" component={ScoreScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

const screenOptionStyle = {
  headerShown: false,
};

export default App;
