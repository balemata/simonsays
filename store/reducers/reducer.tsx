import AsyncStorage from '@react-native-async-storage/async-storage';

import {
  State,
  ActionTypes,
  RESET_GAME,
  ADD_COLOR_TO_COMPUTER_SEQUENCE,
  STORE_PLAYER_SCORE,
} from '../types/types';

const initialState: State = {
  computerSequence: [],
  playersBestTenResults: [],
  reducer: Function,
};

export function reducer(state = initialState, payload: ActionTypes): State {
  switch (payload.type) {
    case ADD_COLOR_TO_COMPUTER_SEQUENCE:
      return {
        ...state,
        computerSequence: [...state.computerSequence, payload.color],
        playersBestTenResults: [...state.playersBestTenResults],
      };

    case RESET_GAME:
      return {
        ...state,
        computerSequence: [],
        playersBestTenResults: [...state.playersBestTenResults],
      };

    case STORE_PLAYER_SCORE:
      if (state.playersBestTenResults.length === 10) {
        var min = Math.min(...state.playersBestTenResults);
        const index = state.playersBestTenResults.indexOf(min);
        if (index > -1) {
          state.playersBestTenResults.splice(index, 1);
        }
        state.playersBestTenResults.sort((a, b) => a - b);
      }
      return {
        ...state,
        computerSequence: [...state.computerSequence],
        playersBestTenResults: [...state.playersBestTenResults, payload.score],
      };

    default:
      return state;
  }
}
