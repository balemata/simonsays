export const RESET_GAME = 'RESET_GAME';
export const UPDATE_COMPUTER_SEQUENCE = 'UPDATE_COMPUTER_SEQUENCE';
export const ADD_COLOR_TO_COMPUTER_SEQUENCE = 'ADD_COLOR_TO_COMPUTER_SEQUENCE';
export const STORE_PLAYER_SCORE = 'STORE_PLAYER_SCORE';

export interface State {
  computerSequence: number[];
  playersBestTenResults: number[];
  reducer: any;
}

interface AddColorToComputerSequence {
  type: typeof ADD_COLOR_TO_COMPUTER_SEQUENCE;
  color: number;
}

interface ResetGame {
  type: typeof RESET_GAME;
}

interface storePlayerScore {
  type: typeof STORE_PLAYER_SCORE;
  score: number;
}

export type ActionTypes =
  | ResetGame
  | AddColorToComputerSequence
  | storePlayerScore;
