import {
  RESET_GAME,
  ADD_COLOR_TO_COMPUTER_SEQUENCE,
  STORE_PLAYER_SCORE,
  ActionTypes,
} from '../types/types';

export const resetGame = (): ActionTypes => {
  return {
    type: RESET_GAME,
  };
};

export const addColorToComputerSequence = (color: number): ActionTypes => {
  return {
    type: ADD_COLOR_TO_COMPUTER_SEQUENCE,
    color,
  };
};

export const storePlayerScore = (score: number): ActionTypes => {
  return {
    type: STORE_PLAYER_SCORE,
    score,
  };
};
